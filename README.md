# Rafael

A rework of the Jera zola theme template.

# Todo

- [ ] Add syntax highlighter prism.js. Reasons: https://lea.verou.me/2012/07/introducing-prism-an-awesome-new-syntax-highlighter/#more-1841
- [ ] Create a three variant of the themes with one default: oolong (light theme), matcha (default), heicha (dark theme). All of these themes are unrelated from each other.
- [ ] Make it as simple but extensible.
